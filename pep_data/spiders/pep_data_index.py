# -*- coding: utf-8 -*-
import scrapy
import json


class PepDataIndexSpider(scrapy.Spider):
    name = 'pep_data_index'
    base_url = 'https://acch.kpk.go.id/pengumuman-lhkpn/index.php/home/loaddata'
    record_per_page = 500
    total_pages = {}
    keyword_counter = {}

    def __init__(self):
        keywords = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
        for keyword in keywords:
            self.keyword_counter[keyword] = 0;
            self.total_pages[keyword] = 0

    
    def map_keyword_to_request(self, keyword, callback, start = 0):
        return scrapy.FormRequest(self.base_url,
                                   formdata={
                                    'draw': '6', 
                                    'nama': keyword, 
                                    'lembaga': '', 
                                    'pelaporan1': '', 
                                    'pelaporan2': '', 
                                    'start': str(start), 
                                    'length': str(self.record_per_page), 
                                    'nhk': '', 
                                    'issearch': '2'},
                                   callback=callback, meta={'keyword': keyword})

    def start_requests(self):
        keywords = list('ABCDEFGHIJKLMNOPQRSTUVWXYZ')
        return [ self.map_keyword_to_request(keyword, self.parse_first_to_get_records_length, 0) for keyword in keywords ]
 
    def parse_first_to_get_records_length(self, response):
        current_keyword = response.meta['keyword']
        json_data = json.loads(response.text)
        records_total = int(json_data['recordsTotal'])

        for start in range(0, records_total, self.record_per_page):
            yield self.map_keyword_to_request(current_keyword, self.parse, start)

    def parse(self, response):
        current_keyword = response.meta['keyword']
        json_data = json.loads(response.text)
        data_array = json_data['data']
        for item_data in data_array:
            yield {
                'name' : item_data[0].strip(),
                'institution' : item_data[1].strip(),
                'position' : item_data[2].strip(),
                'nhk' : item_data[3].strip(),
                'birth_date' : item_data[4].strip()
            }

