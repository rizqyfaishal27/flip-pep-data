from __future__ import print_function
from datetime import date, datetime, timedelta
import mysql.connector
import json

cnx = mysql.connector.connect(user='root', password='root', database='pep_data')
cursor = cnx.cursor()

tomorrow = datetime.now().date() + timedelta(days=1)

query = "INSERT INTO Persons (name, institution, position, nhk) VALUES ('%s', '%s', '%s', '%s')"

with open('data.jl', 'rb') as file:
	data_from_file = file.readlines()
	for row in data_from_file:
		try:
			row_as_json = json.loads(row) 
			data_person = (
				row_as_json['name'].replace('\'', ''),
				row_as_json['institution'].replace('\'', ''),
				row_as_json['position'].replace('\'', ''),
				row_as_json['nhk'],
			)
			query_execute = query % data_person
			print(query_execute)
			cursor.execute(query_execute)
			cnx.commit()
		except Exception as error:
			print(error)


cursor.close()
cnx.close()
